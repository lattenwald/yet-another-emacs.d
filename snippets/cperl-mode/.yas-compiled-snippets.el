;;; Compiled snippets and support files for `cperl-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'cperl-mode
                     '(("script1" "#!/usr/bin/env perl\nuse strict;\nuse warnings;\nuse utf8;\n\nuse Carp;\n\nsub init {\n	${1:#TODO init}\n}\n\nsub main {\n	${2:#TODO do stuff}\n}\n\ninit();\nmain();\n" "Simple Perl script" nil nil nil nil nil nil)
                       ("script2" "#!/usr/bin/env perl\nuse strict;\nuse warnings;\nuse utf8;\n\nuse Carp;\nuse Getopt::Std;\n\nour $VERSION = '1.0';\n$Getopt::Std::STANDARD_HELP_VERSION = 1;\n\nmy %set_opt = (\n	${1:#TODO set options}\n	);\n\nsub init {\n	my %opts;\n	unless(getopts('${0:options}', \\%opts)) { #TODO implement\n		usage();\n		exit 1;\n	}\n\n	while (my ($opt, $val) = each %opts) {\n		if ($set_opt{$opt}) {\n			$set_opt{$opt}->($val);\n		} else {\n			croak('Unknown option: ' . $opt);\n		}\n	}\n\n	# other\n	#Runtime::init(1);\n}\n\nsub main {\n	${3:#TODO do stuff}\n}\n\nsub usage {\n	croak 'Unimplemented'; #TODO implement\n	my $usage = <<\"USAGE\";\nUsage: \\$0 OPTIONS\nAvailable options:\n  -h  show this message\n  ${2:help message}...\nUSAGE\n	print STDERR $usage;\n}\n\nsub HELP_MESSAGE() {usage()}\n\ninit();\nmain();\n" "Perl script with Getopt::Std" nil nil nil nil nil nil)))


;;; Do not edit! File generated at Mon Jun 30 16:01:44 2014
