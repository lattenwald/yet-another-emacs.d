;; (define-prefix-command 'mode-specific-map)
;; (global-set-key (kbd "C-c") 'mode-specific-map)

(add-to-list 'load-path "~/.emacs.d/elisp")
(package-initialize)

;; custom file
(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file 'noerror)

;;; File associations
(add-to-list 'auto-mode-alist '("\\.xc\\'" . js-mode))       ;; modxvm's .xc -> js-mode
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode)) ;; .md -> markdown-mode
(add-to-list 'auto-mode-alist '("\\.m$"    . octave-mode))   ;; .m -> octave-mode

;;; use super as meta
(setq x-super-keysym 'meta)

;;; fullscreen
(defun toggle-fullscreen ()
  "Toggle full screen on X11"
  (interactive)
  (when (eq window-system 'x)
    (set-frame-parameter
     nil 'fullscreen
     (when (not (frame-parameter nil 'fullscreen)) 'fullboth))))

(global-set-key [f11] 'toggle-fullscreen)

;;; align
(require 'align)
(defadvice align-regexp (around align-regexp-with-spaces activate)
  (let ((indent-tabs-mode nil))
    ad-do-it))

;;; ido mode
(require 'ido)
(ido-mode t)
(autoload 'idomenu "idomenu" nil t)

;;; haskell stuff
(require 'hs-lint)
(require 'shm)

(when (file-exists-p "~/src/stack-ide")
  (add-to-list 'load-path "~/src/stack-ide/stack-mode/")
  (require 'stack-mode)
  (add-hook 'haskell-mode-hook 'stack-mode))

(eval-after-load "structured-haskell-mode"
  '(progn
     (define-key shm-map (kbd "RET") 'shm/newline-indent)
     ;; (define-key shm-map (kbd "C-j") 'shm/simple-indent-newline-same-col)
     (define-key shm-map (kbd "C-j") 'shm/newline-indent)
     (define-key shm-map (kbd "DEL") nil)
     (define-key shm-map (kbd ")") nil)
     (define-key shm-map (kbd "<deletechar>") nil)
     (define-key shm-map (kbd "S-DEL") 'shm/del)
     ))

(eval-after-load "haskell-mode"
  '(progn
     (define-key haskell-mode-map (kbd "C-,") 'haskell-move-nested-left)
     (define-key haskell-mode-map (kbd "C-.") 'haskell-move-nested-right)
     (define-key haskell-mode-map (kbd "RET") nil)))

(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
;; (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
(add-hook 'haskell-mode-hook 'structured-haskell-mode)

(eval-after-load "stack-mode"
  '(progn
     (define-key stack-mode-map (kbd "C-c C-l") nil)
     (define-key stack-mode-map (kbd "C-c C-r") 'stack-mode-load)))

(add-to-list 'align-rules-list
             '(haskell-types
               (regexp . "\\(\\s-+\\)\\(::\\|∷\\)\\s-+")
               (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
             '(haskell-assignment
               (regexp . "\\(\\s-+\\)=\\s-+")
               (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
             '(haskell-arrows
               (regexp . "\\(\\s-+\\)\\(->\\|→\\)\\s-+")
               (modes quote (haskell-mode literate-haskell-mode))))
(add-to-list 'align-rules-list
             '(haskell-left-arrows
               (regexp . "\\(\\s-+\\)\\(<-\\|←\\)\\s-+")
               (modes quote (haskell-mode literate-haskell-mode))))

(eval-after-load "which-func"
  '(add-to-list 'which-func-modes 'haskell-mode))

;;; elm-mode
(add-hook 'elm-mode-hook 'turn-on-elm-indentation)

;;; helm
;; (global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-x") 'helm-M-x)

;;; projectile
(require 'helm-projectile)
(helm-projectile-on)

;;; octave
(autoload 'octave-mode "octave-mod" nil t)

;;; org-mode
(eval-after-load "org"
  '(progn
     (define-key org-mode-map (kbd "<M-left>") nil)
     (define-key org-mode-map (kbd "<M-right>") nil)
     (define-key org-mode-map (kbd "<M-up>") nil)
     (define-key org-mode-map (kbd "<M-down>") nil)
     (define-key org-mode-map (kbd "<C-M-left>") 'org-metaleft)
     (define-key org-mode-map (kbd "<C-M-right>") 'org-metaright)
     (define-key org-mode-map (kbd "<C-M-up>") 'org-metaup)
     (define-key org-mode-map (kbd "<C-M-down>") 'org-metadown)
     (define-key org-mode-map (kbd "<C-M-return>") 'org-insert-heading)
     ))

;;; fixmee-mode
(setq-default fixmee--listview-local-only t)

;;; sudo save
(defun sudo-save ()
  (interactive)
  (if (not buffer-file-name)
      (write-file (concat "/sudo:root@localhost:" (ido-read-file-name "File:")))
    (write-file (concat "/sudo:root@localhost:" buffer-file-name))))

;;; sqlplus
(require 'sqlplus)
(setenv "NLS_LANG" "RUSSIAN_CIS.CL8MSWIN1251")
(setenv "NLS_NUMERIC_CHARACTERS" "._")
(add-hook 'sqlplus-mode-hook
	  (lambda ()
	    (set-process-coding-system
	     (get-process (sqlplus-get-process-name sqlplus-connect-string))
	     'cp1251 'cp1251)))

(defun sql-orcl-test ()
  (interactive)
  (sqlplus "prophet/prophet@188.93.60.196/ORCL" "orcl" t))


;;; large files
(require 'vlf-setup)

;;; keys
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "C-x C-z"))
(global-set-key (kbd "M-[") 'align)
(global-set-key (kbd "C-c =") 'magit-status)
(global-set-key (kbd "<home>") 'beginning-of-line)
(global-set-key (kbd "<end>") 'end-of-line)

;;; magit
(setq magit-last-seen-setup-instructions "1.4.0")
(defadvice magit-diff (after switch-to-diff activate)
  (other-window 1))

;;; whitespace-cleanup on save
(add-hook 'before-save-hook 'whitespace-cleanup)

;;; html stuff
(require 'multi-web-mode)
(setq mweb-default-major-mode 'html-mode)
(setq mweb-tags '((php-mode "<\\?php\\|<\\? \\|<\\?=" "\\?>")
		  (js-mode "<script +\\(type=\"text/javascript\"\\|language=\"javascript\"\\)[^>]*>" "</script>")
		  (css-mode "<style +type=\"text/css\"[^>]*>" "</style>")
	  (scheme-mode "<\\?scm" "\\?>")))
(setq mweb-filename-extensions '("eguile" "php" "htm" "html" "ctp" "phtml" "php4" "php5"))
(multi-web-global-mode 1)

;;; GUI
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; (load-theme 'hickey t)
(load-theme 'wombat)
(menu-bar-mode 0)
(tool-bar-mode 0)
(column-number-mode t)
(cua-mode 1)

;;; undo tree stuff
(defalias 'redo 'undo-tree-redo)
(global-set-key (kbd "C-r") 'redo)
(global-set-key (kbd "C-z") 'undo)

;; Moving between windows
(global-set-key (kbd "M-<left>") 'windmove-left)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<up>") 'windmove-up)
(global-set-key (kbd "M-<down>") 'windmove-down)

;; misc
(global-set-key (kbd "C-c i") 'idomenu)
(global-set-key (kbd "C-'") 'comment-or-uncomment-region)
(global-set-key (kbd "C-c o") 'occur)

;; tabs, indentation and stuff
(setq-default indent-tabs-mode nil)
(autoload 'smart-tabs-mode "smart-tabs-mode")
;;  "Intelligently indent with tabs, align with spaces!")
(autoload 'smart-tabs-mode-enable "smart-tabs-mode")
(autoload 'smart-tabs-advice "smart-tabs-mode")
(autoload 'smart-tabs-insinuate "smart-tabs-mode")

(smart-tabs-insinuate 'c 'javascript 'cperl 'python 'ruby 'nxml)

;; cperl-mode
(defalias 'perl-mode 'cperl-mode)

; perlbrew
(require 'perlbrew-mini)
(perlbrew-mini-use-latest)

(require 'perl-quote)

;;; geiser mode
(add-hook 'geiser-repl-mode-hook
          (lambda ()
            (local-(setq  )et-key (kbd "C-d") 'geiser-repl-exit)))

;;; terminal
(defadvice ansi-term (after advise-ansi-term-coding-system)
  (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
(ad-activate 'ansi-term)
(add-hook 'term-mode-hook ())

;;; incremeting numbers
(defun increment-number-at-point ()
  (interactive)
  (skip-chars-backward "0123456789")
  (or (looking-at "[0123456789]+")
      (error "No number at point"))
  (replace-match (number-to-string (1+ (string-to-number (match-string 0))))))

(global-set-key (kbd "C-c +") 'increment-number-at-point)

;;; emacs server
(server-start)

;;; PATH
(exec-path-from-shell-initialize)

